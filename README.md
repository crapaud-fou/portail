La page d'accueil de la mare des crapauds fous
===============================================

Ceci est la page d'atterrissage du site des crapauds fous.

Pour bosser localement, lancez `python -m http.server` a la base du repo et puis `open http://localhost:8000`

Pour toute demande d'info passez donc sur <https://coa.crapaud-fou.org> et demandez dans le canal #animation-crapauds

mose
