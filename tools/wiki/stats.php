
<?php
# copy this at the root of tiki

require 'db/local.php';

$db = new mysqli('localhost', $user_tiki, $pass_tiki, $dbs_tiki);

if ($db->connect_errno) {
  echo mysqli_connect_error();
  exit;
}

$db->set_charset("utf8");

function getone($query) {
  global $db;
  $result = $db->query($query);
  return $result->fetch_row()[0];
}
function getlist($query) {
  global $db;
  $result = $db->query($query);
  $back = array();
  while ($row = $result->fetch_row()) {
    $back[$row[0]] = $row[1];
  }
  return $back;
}

$interval = "unix_timestamp(date_sub(current_date(), interval 30 day))";

$data = array(
  'crapauds' => array(
    'total' => 0,
    'recent' => 0
  ),
  'pages' => array(
    'total' => 0,
    'recent' => 0,
    'liste' => array()
  ),
  'photos' => array(
    'total' => 0,
    'recent' => 0
  )
);

$data['crapauds']['total'] = getone("select count(*) from users_users");
$data['crapauds']['recent'] = getone("select count(*) from users_users where currentLogin > $interval");

$data['pages']['total'] = getone("select count(*) from tiki_pages");
$data['pages']['recent'] = getone("select count(*) from tiki_pages where lastModif > $interval");

$data['photos']['total'] = getone("select count(*) from tiki_files");
$data['photos']['recent'] = getone("select count(*) from tiki_files where created > $interval");

require_once('tiki-setup_base.php');
$pages = $tikilib->list_pages(0, 10, 'lastModif_desc');
foreach ($pages["data"] as $page) {
  $data['pages']['liste'][$page["pageSlug"]] = $page["pageName"];
}

header('Content-Type: application/json');
$json = json_encode( (object) $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

if ($json === false) {
  $json = json_encode(array("jsonError", json_last_error_msg()));
}

echo $json;