<?php
# copy this at the root of tiki

require 'db/local.php';
setlocale(LC_ALL, "fr_FR.UTF8");
date_default_timezone_set("Europe/Paris");

$db = new mysqli('localhost', $user_tiki, $pass_tiki, $dbs_tiki);

if ($db->connect_errno) {
  echo mysqli_connect_error();
  exit;
}

$db->set_charset("utf8");

function getlist($query) {
  global $db;
  $back = array();
  if ($result = $db->query($query)) {
    while ($row = $result->fetch_row()) {
      $back[$row[0]] = $row[1];
    }
  }
  return $back;
}

$fields = array(
  '3' => "titre",
  '5' => "wikipage",
  '7' => "lieu"
);

$data = array(
  'avenir' => array(),
  'passe' => array()
);

$avenir = "select itemId, value from tiki_tracker_item_fields where fieldId=1 and datediff(date(from_unixtime(
  value)), date(now())) >= -1 order by value asc limit 5";
$passe = "select itemId, value from tiki_tracker_item_fields where fieldId=1 and datediff(date(from_unixtime(
  value)), date(now())) < -1 order by value desc limit 5";

function getevents($list) {
  global $fields;
  $back = array();
  foreach ($list as $id => $date) {
    $d = date_create();
    localtime(date_timestamp_set($d, $date));
    $event = array(
      'id' => $id,
      'date' => strftime("%A %e %B %Y", $date)
    );
    $query = "select fieldId, value from tiki_tracker_item_fields where itemId=$id and fieldId in (".join(",", array_keys($fields)).")";
    foreach (getlist($query) as $key => $value) {
      $event[$fields[$key]] = $value;
    }
    $back[] = $event;
  }
  return $back;
}

$avenir = getevents(getlist($avenir));
$passe = getevents(getlist($passe));
$data = array(
  "avenir" => $avenir,
  "passe" => $passe
);

header('Content-Type: application/json');

$json = json_encode( (object) $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

if ($json === false) {
  $json = json_encode(array("jsonError", json_last_error_msg()));
}

echo $json;
