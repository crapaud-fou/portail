<?php
# copy this at the root of q2a

require 'qa-config.php';

$db = new mysqli(QA_MYSQL_HOSTNAME, QA_MYSQL_USERNAME, QA_MYSQL_PASSWORD, QA_MYSQL_DATABASE);

if ($db->connect_errno) {
  echo mysqli_connect_error();
  exit;
}

$db->set_charset("utf8");

function getone($query) {
  global $db;
  $result = $db->query($query);
  return $result->fetch_row()[0];
}
function getlist($query) {
  global $db;
  $result = $db->query($query);
  $back = array();
  while ($row = $result->fetch_row()) {
    $back["/$row[0]"] = $row[1];
  }
  return $back;
}

$interval = "date_sub(current_date(), interval 30 day)";

$data = array(
  'crapauds' => array(
    'total' => 0,
    'recent' => 0
  ),
  'idees' => array(
    'total' => 0,
    'recent' => 0,
    'liste' => array()
  ),
  'commentaires' => array(
    'total' => 0,
    'recent' => 0
  )
);

$data['crapauds']['total'] = getone("select count(*) from qa_users");
$data['crapauds']['recent'] = getone("select count(*) from qa_users where loggedin > $interval");

$data['idees']['total'] = getone("select count(*) from qa_posts where type = 'Q'");
$data['idees']['recent'] = getone("select count(*) from qa_posts where type = 'Q' and created > $interval");
$data['idees']['liste'] = getlist("select postid, title from qa_posts where type='Q' order by created desc limit 10");

$data['commentaires']['total'] = getone("select count(*) from qa_posts where type = 'A' or type = 'C'");
$data['commentaires']['recent'] = getone("select count(*) from qa_posts where (type = 'A' or type = 'C') and created > $interval");

header('Content-Type: application/json');
$json = json_encode( (object) $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

if ($json === false) {
  $json = json_encode(array("jsonError", json_last_error_msg()));
}

echo $json;
