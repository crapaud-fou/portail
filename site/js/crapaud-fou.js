$(document).ready(function() {
  // chat
  $.getJSON("data/chat.json", function(data) {
    $(".chat .total .crapauds").text(data.crapauds.total);
    $(".chat .total .canaux").text(data.canaux.total);
    $(".chat .total .messages").text(data.messages.total);
    $(".chat .recent .crapauds").text(data.crapauds.recent);
    $(".chat .recent .canaux").text(data.canaux.recent);
    $(".chat .recent .messages").text(data.messages.recent);
    $(".chat ul").text("");
    $.each(data.canaux.liste, function(i, canal) {
      var link = $("<a>")
        .attr("href", "https://coa.crapaud-fou.org/channel/" + canal)
        .attr("class", "list-item")
        .text(canal);
      var li = $("<li>").append(link);
      $(".chat ul").append(li);
    });
    $.each(data.cohortes, function(i, canal) {
      var links = $("<a>")
        .attr("href", "https://coa.crapaud-fou.org/channel/cohorte-" + canal)
        .attr("class", "badge badge-success mr-1")
        .text(canal);
      $(".cohortes").append(links);
    });
  });

  // idees
  $.getJSON("data/idees.json", function(data) {
    $(".idees .total .crapauds").text(data.crapauds.total);
    $(".idees .total .idees").text(data.idees.total);
    $(".idees .total .commentaires").text(data.commentaires.total);
    $(".idees .recent .crapauds").text(data.crapauds.recent);
    $(".idees .recent .idees").text(data.idees.recent);
    $(".idees .recent .commentaires").text(data.commentaires.recent);
    $(".idees ul").text("");
    $.each(data.idees.liste, function(index, idee) {
      var link = $("<a>")
        .attr("href", "https://idees.crapaud-fou.org" + index)
        .attr("class", "list-item")
        .attr("title", idee)
        .text(idee);
      var li = $("<li>").append(link);
      $(".idees ul").append(li);
    });
  });

  // tiki
  $.getJSON("data/wiki.json", function(data) {
    $(".wiki .total .crapauds").text(data.crapauds.total);
    $(".wiki .total .pages").text(data.pages.total);
    $(".wiki .total .photos").text(data.photos.total);
    $(".wiki .recent .crapauds").text(data.crapauds.recent);
    $(".wiki .recent .pages").text(data.pages.recent);
    $(".wiki .recent .photos").text(data.photos.recent);
    $(".wiki ul").text("");
    $.each(data.pages.liste, function(index, page) {
      var link = $("<a>")
        .attr("href", "https://wiki.crapaud-fou.org/" + index)
        .attr("class", "list-item")
        .attr("title", page)
        .text(page);
      var li = $("<li>").append(link);
      $(".wiki ul").append(li);
    });
  });

  // rencontres
  $.getJSON("data/rencontres.json", function(data) {
    $.each(data.avenir, function(index, rencontre) {
      var quand = $("<span>")
        .attr("class", "date")
        .text(rencontre.date);
      var ou = $("<span>")
        .attr("class", "lieu float-right")
        .text(rencontre.lieu);
        
        if (rencontre.wikipage === "") {
          rencontre.wikipage = "tiki-view_tracker_item.php?itemId=" + rencontre.id + "&from=Agenda";
        }
        var titre = $("<a>")
        .attr("href", "https://wiki.crapaud-fou.org/" + rencontre.wikipage)
        .attr("class", "list-item")
        .attr("title", rencontre.titre)
        .text(rencontre.titre);
      var div = $("<div>").append(quand).append(ou).append($("<br>")).append(titre);
      $(".avenir ul").append(div);
    });
    $.each(data.passe, function(index, rencontre) {
      var quand = $("<span>")
        .attr("class", "date")
        .text(rencontre.date);
      var ou = $("<span>")
        .attr("class", "lieu float-right")
        .text(rencontre.lieu);
      if (rencontre.wikipage === "") {
        rencontre.wikipage = "tiki-view_tracker_item.php?itemId=" + rencontre.id + "&from=Agenda";
      }
      var titre = $("<a>")
        .attr("href", "https://wiki.crapaud-fou.org/" + rencontre.wikipage)
        .attr("class", "list-item")
        .attr("title", rencontre.titre)
        .text(rencontre.titre);
      var div = $("<div>").append(quand).append(ou).append($("<br>")).append(titre);
      $(".passe ul").append(div);
    });
  });

  // cratube
  $.getJSON("data/tube.json", function(data) {
    $.each(data.data.slice(0, 10), function(index, video) {
      var link = $("<a>")
      .attr("href", "https://tube.crapaud-fou.org/w/" + video.shortUUID)
      .attr("class", "list-item")
      .attr("title", video.name)
      .text(video.name);
      var li = $("<li>").append(link);
      $(".tube ul").append(li);
      $(".lastvideo")
        .attr("src", "https://tube.crapaud-fou.org" + data.data[0].embedPath)
        .attr("title", data.data[0].name);
    });
  });

  // link coastats
  $(".coastats").on("click", function() {
    window.location.href = "coastats.html";
  });

  // global
  $.getJSON("data/global.json", function(data) {
    $("#lastupdate").text(data.lastupdate);
  });

  // hash navigation
  var hash = window.location.hash.substr(1);
  if ($("#help" + hash).length) {
    $("#help" + hash).modal('show');
  }
});