$(document).ready(function () {

  $.getJSON("data/chat_stat_monthly.json", function (data) {
    updated = "mise à jour " + data.updated;
    labels = data.labels;
    console.log(data);
    graphDisplay($('#byChannel'), data.messagesByChannel, "Nombre de messages par canal sur 1 an (" + updated + ")");
    graphDisplay($('#byTopic'), data.messagesByTopic, "Nombre de messages par thème sur 1 an (" + updated + ")");
    graphDisplay($("#usersByChannel"), data.usersByChannel, "Nombre d'utilisateurs actifs par canal sur 1 an (" + updated + ")");
    graphDisplay($("#usersGlobal"), data.usersGlobal, "Nombre d'utilisateurs actifs sur 1 an (" + updated + ")");
  });

  $.getJSON("data/chat_stat_daily.json", function (data) {
    updated = "mise à jour " + data.updated;
    labels = data.labels;
    graphDisplay($('#byChannel_daily'), data.messagesByChannel, "Nombre de messages par canal sur 30 jours (" + updated + ")");
    graphDisplay($('#byTopic_daily'), data.messagesByTopic, "Nombre de messages par thème sur 30 jours (" + updated + ")");
    graphDisplay($("#usersByChannel_daily"), data.usersByChannel, "Nombre d'utilisateurs actifs par canal sur 30 jours (" + updated + ")");
    graphDisplay($("#usersGlobal_daily"), data.usersGlobal, "Nombre d'utilisateurs actifs sur 30 jours (" + updated + ")");
  });

});

function graphDisplay(ctx, data, title) {
  var myChart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
      labels: labels,
      datasets: data,
    },

    // Configuration options go here
    options: {
      legend: {
        display: false
      },
      title: {
        display: true,
        text: title,
        position: "top"
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            stepSize: 500
          }
        }]
      },
      onClick: function (evt, elem) {
        var firstPoint = myChart.getElementAtEvent(evt)[0];

        if (firstPoint) {
            var label = myChart.data.datasets[firstPoint._datasetIndex].label;
            window.open("https://coa.crapaud-fou.org/channel/"+label);
        }
      }
    }
  });
}