# coding: utf8

# toutes les chaines sont en unicode (même les docstrings)
from __future__ import unicode_literals

# from pprint import pprint
from rocketchat_API.rocketchat import RocketChat
import json
import os
import random
import time
from datetime import datetime
from monthdelta import monthdelta
from common.rocketchathelper import getAllChannels, getAllMessages, Connection
from common.savehelper import save
from common.charthelper import createElement, getColor, setTopicsInfo, getTopics

begin = datetime.now()
end = datetime.now()

def main():
  
  global begin, end
  
  rocket = Connection()
  
  labels = [None] * 12
  messagesByChannel = []
  usersByChannel = []
  messagesByTopic = []
  usersGlobal = []

  now = datetime.now()
  date = datetime(now.year, now.month, now.day, 0,0,0)

  info = {
    "updated": "{:02}/{:02}/{:04}".format(now.day, now.month, now.year),
    "labels": labels,
    "messagesByChannel": messagesByChannel,
    "usersByChannel": usersByChannel,
    "messagesByTopic": messagesByTopic,
    "usersGlobal": usersGlobal
  }

  uniqueUserGlobal = [None] * 12

  waittest = 0
  idChannel = 0
  channels = getAllChannels(rocket)
  for channel in channels:
    idChannel += 1
    if channel['name'].startswith("cohorte"):
      continue

    dataMess = []
    dataUsers = []
    print( channel['name'] )
    begin = date - monthdelta(12)
    end = begin + monthdelta(1)

    messages = getAllMessages(rocket, channel['_id'], begindate= begin, enddate=date, count= channel['msgs'])
    if messages is None:
      print("ERROR to get messages")
      break

    topics = getTopics(channel)    
    print(topics)
    for id in range(0, 12):
      if uniqueUserGlobal[id] == None:
        uniqueUserGlobal[id] = []
      labels[id] = begin.strftime("%b %Y")
      print("\t{0}".format(labels[id]))

      resultMess = list(filter(lambda mess: begin < datetime.strptime(mess["ts"], "%Y-%m-%dT%H:%M:%S.%fZ") < end, messages))
      length = len(resultMess)
      dataMess.append(length)

      if length > 0:
        setTopicsInfo(topics, messagesByTopic, id, length)

        users = []
        for mess in resultMess:
          users.append(mess['u']['_id'])
          uniqueUserGlobal[id].append(mess['u']['_id'])
        usersDistinct = set(users)
        dataUsers.append(len(usersDistinct))
      else:
        dataUsers.append(0)

      begin = end
      end = begin + monthdelta(1)
  
    color = getColor()
    messageByChannel = createElement(channel['name'], color,dataMess)
    userByChannel = createElement( channel['name'], color,dataUsers)

    messagesByChannel.append(messageByChannel)
    usersByChannel.append(userByChannel)
    waittest += 1
    if waittest >= 5:
      print("Wait {}/{}".format(idChannel, len(channels)))
      waittest = 0
      time.sleep(30)

  for id in range(0, 12):
    uniqueUserGlobal[id] = len(set(uniqueUserGlobal[id]))

  userGlobal = createElement( 'global', 'red', uniqueUserGlobal)
  usersGlobal.append(userGlobal)

  save(info, "chat_stat_monthly")

if __name__ == "__main__":
    main()