# coding: utf8

# toutes les chaines sont en unicode (même les docstrings)
from __future__ import unicode_literals

# from pprint import pprint
from rocketchat_API.rocketchat import RocketChat
import json
import os
import random
import time
from datetime import datetime, timedelta
from common.rocketchathelper import getAllChannels, getAllMessages, Connection
from common.savehelper import save
from common.charthelper import createElement, getColor, setTopicsInfo, getTopics

begin = datetime.now()
end = datetime.now()

def main():
  rocket = Connection()
  nbElements = 30

  global begin, end
  
  labels = [None] * nbElements
  messagesByChannel = []
  usersByChannel = []
  messagesByTopic = []
  usersGlobal = []

  now = datetime.now()
  date = datetime(now.year, now.month, now.day, 0,0,0)

  info = {
    "updated": "{:02}/{:02}/{:04}".format(now.day, now.month, now.year),
    "labels": labels,
    "messagesByChannel": messagesByChannel,
    "usersByChannel": usersByChannel,
    "messagesByTopic": messagesByTopic,
    "usersGlobal": usersGlobal
  }

  uniqueUserGlobal = [None] * nbElements

  idChannel = 0
  channels = getAllChannels(rocket)
  for channel in channels:
    idChannel += 1
    if channel['name'].startswith("cohorte"):
      continue

    dataMess = []
    dataUsers = []
    print( channel['name'] )
    begin = date - timedelta(nbElements)
    end = begin + timedelta(nbElements)

    topics = getTopics(channel)    
    print(topics)
    messages = getAllMessages(rocket, channel['_id'], begindate= begin, enddate= end, count= channel["msgs"])
    if messages is None:
      print("ERROR to get messages")
      break

    end = begin + timedelta(1)

    for id in range(0, nbElements):
      if uniqueUserGlobal[id] == None:
        uniqueUserGlobal[id] = []
      labels[id] = begin.strftime("%x")
      print("\t{0}".format(labels[id]))
      
      resultMess = list(filter(filter_message, messages))
      length = len(resultMess)
      dataMess.append(length)

      if length > 0:
        setTopicsInfo(topics, messagesByTopic, id, length)

        users = []
        for mess in resultMess:
          users.append(mess['u']['_id'])
          uniqueUserGlobal[id].append(mess['u']['_id'])
        usersDistinct = set(users)
        dataUsers.append(len(usersDistinct))
      else:
        dataUsers.append(0)

      begin = end
      end = begin + timedelta(1)
  
    color = getColor()
    messageByChannel = createElement(channel['name'], color,dataMess)
    userByChannel = createElement( channel['name'], color,dataUsers)

    messagesByChannel.append(messageByChannel)
    usersByChannel.append(userByChannel)
    print("Wait {}/{}".format(idChannel, len(channels)))
    time.sleep(30)


  for id in range(0, nbElements):
    uniqueUserGlobal[id] = len(set(uniqueUserGlobal[id]))

  userGlobal = createElement( 'global', 'red', uniqueUserGlobal)
  usersGlobal.append(userGlobal)

  save(info, "chat_stat_daily")

def filter_message(mess):
  if 't' in mess:
    return False

  date = datetime.strptime(mess["ts"], "%Y-%m-%dT%H:%M:%S.%fZ")

  if date < begin:
    return False
  
  if end < date:
    return False

  return True

if __name__ == "__main__":
    main()