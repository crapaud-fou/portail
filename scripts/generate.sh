#!/bin/sh

# idees
wget -O ./site/data/idees.json https://idees.crapaud-fou.org/stats.php

# wiki
wget -O ./site/data/wiki.json https://wiki.crapaud-fou.org/stats.php

# rencontres
wget -O ./site/data/rencontres.json https://wiki.crapaud-fou.org/rencontres.php

# tube
wget -O ./site/data/tube.json "https://tube.crapaud-fou.org/api/v1/videos?sort=-publishedAt&isLocal=1&count=10"

# chat
# wget -O ./site/data/chat_stat_monthly.json https://idees.crapaud-fou.org/data/chat_stat_monthly.json
# wget -O ./site/data/chat_stat_daily.json https://idees.crapaud-fou.org/data/chat_stat_daily.json
# wget -O ./site/data/chat.json https://idees.crapaud-fou.org/data/chat.json

# global
# pip3 install -r ./scripts/requirements.txt
python3 ./scripts/generate_global_info.py
