from rocketchat_API.rocketchat import RocketChat
from datetime import datetime
import os

def Connection():
  return RocketChat(None, None, auth_token= os.environ['ROCKETCHAT_AUTH'], user_id= os.environ['ROCKETCHAT_USERID'], server_url=os.environ['ROCKETCHAT_SERVER'])

def getNodesOrigin(channel):
  nodes = []
  if 'description' not in channel:
    nodes.append("global")
    return nodes

  if channel['description'].find("#projet") != -1:
    nodes.append("project")
  if channel['description'].find("#democratie") != -1:
    nodes.append("democratie")
  if channel['description'].find("#ecologie") != -1:
    nodes.append("ecologie")
  if channel['description'].find("#technologie") != -1:
    nodes.append("technologie")

  if not nodes:
    nodes.append("global")

  return nodes

def getAllChannels(rocket):
  index = 0
  allChannels = []
  while True:
    channels = rocket.channels_list(offset= index).json()
      
    allChannels.extend([ channel for channel in channels['channels'] if 'archived' not in channel])
    if channels['count'] + channels['offset'] >= channels['total']:
      break
    index += channels['count']
  return allChannels

def getAllMessages(rocket, roomid, begindate=None, enddate=datetime.now(), count=10000):
  result = rocket.channels_history(roomid, oldest= begindate, latest= enddate, count= count).json()
  if result["success"]:
    #filter system message
    return list(filter(lambda message: "t" not in message, result["messages"]))    

  return None

if __name__ == "__main__":
    print("Ce fichier est juste une librarie")