import json, os

def save(info, filename):
  # Récupération du répertoire racine du repo
  rootFolder = os.path.join(os.path.dirname(__file__), '..', '..')
  # Répertoire pour stocker le fichier de sortie
  dataFolder = os.path.join(rootFolder, 'site', 'data')

  statsFilePath = os.path.abspath(
      os.path.join(dataFolder, '{}.json'.format(filename)))
  with open(statsFilePath, "w") as file_write:
    json.dump(info, file_write)