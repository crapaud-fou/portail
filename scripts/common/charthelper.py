import random
import re


def getColor():
    r = random.randrange(255)
    g = random.randrange(255)
    b = random.randrange(255)
    return 'rgb({:0},{:0},{:0})'.format(r, g, b)


def createElement(label, color, data):
  return {
      "label": label,
      "backgroundColor": color,
      "data": data
  }


def setTopicsInfo(topics, messagesDataTopic, id, length):
  for topic in topics:
    result = None
    if len(messagesDataTopic) > 0:
      result = next((x for x in messagesDataTopic if x['label'] == topic), None)
    
    if result is None:
      result = createElement(topic, getColor(), [0] * 30)
      messagesDataTopic.append(result)

    result['data'][id] += length


def getTopics(channel):
  if 'description' in channel:
    topics = re.findall(r"#(\w+)\W?", channel['description'])
    if len(topics) == 0:
      topics.append('global')
    return topics
  return ['global']


if __name__ == "__main__":
  print("not executable")
