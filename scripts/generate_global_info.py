import os, json, pytz
from datetime import datetime, timedelta
from common.savehelper import save

def main():

    zone = ' '.join(pytz.country_timezones['fr'])
    tz = pytz.timezone(zone)
    now = datetime.now(tz)

    info = {
        "lastupdate": datetime.strftime(now, "%d/%m/%Y %X")
    }
    save(info, "global")

if __name__ == "__main__":
    main()